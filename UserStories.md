# User Stories

- [x] As a user, I want to register so that I can be able to keep track of my information.
- [x] As a user, I want to log in so that I can be able task questions and give answers to other users.
- [x] As a user, I want my account to be secure so I can be sure nobody else is using it for malicious purposes.
- [x] As a user, I want to be able to ask questions with descriptions in a specific category so that I can get answers to what I need to know.
- [x] As a user, I want to be able to answer questions from other users
- [x] As a user, I want to be able to vote on good and bad answers
- [x] As a user, I want to be able to edit my questions and answers
- [ ] As an administrator/moderator, I want to be able to moderate questions and delete ones that are inappropriate
- [x] As a user, I want to respond to other people’s questions to help them.
- [x] As a user, I want to be able to vote on questions so that the important ones are shown to more people.
- [ ] As a user that has asked a question, I want to be able to choose the most appropriate answer that has helped me solve my problem.
- [ ] As a citizen of a member state of the EU , I want to be able to request, change or delete my data when I want to.
