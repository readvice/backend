package nl.fhict.qaapi.model;

import com.fasterxml.jackson.annotation.*;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.*;

@Getter
@Setter
@Entity
@Table(name = "user")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "id")
public class User {

    @JsonView({View.External.User.class,View.Internal.User.class})
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @JsonView({View.External.User.class,View.Internal.User.class})
    @Column(name = "username", nullable = false, unique = true)
    private String username;

    @JsonView(View.Internal.User.class)
    @Column(name = "email", nullable = false)
    private String email;

    @JsonView(View.Internal.User.class)
    @Column(name = "password", nullable = false)
    private String password;

    @JsonView({View.External.User.class,View.Internal.User.class})
    @Column(name = "score")
    private int score;

    @JsonView(View.External.User.class)
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "user_categories",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "category_id", referencedColumnName = "id")
    )
    private Set<Category> categories = new HashSet<>();

    @JsonView(View.Internal.User.class)
    @Column(name = "mfa")
    private Boolean mfa;

    @JsonView(View.Internal.User.class)
    @JsonIgnore
    @ElementCollection(fetch = FetchType.LAZY)
    private Map<String, String> secrets = new HashMap<>();

    @JsonView(View.Internal.User.class)
    private String roles;

    @JsonView(View.Internal.User.class)
    private String permissions;

    @JsonView({View.External.User.class,View.Internal.User.class})
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    @JsonBackReference
    private List<Question> questions;


    @JsonView({View.External.User.class,View.Internal.User.class})
    @ManyToMany(mappedBy = "downvotes", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    private List<Question> downvotes;

    @JsonView({View.External.User.class,View.Internal.User.class})
    @ManyToMany(mappedBy = "upvotes", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    private List<Question> upvotes;

    public User(String username, String email, String password, int score, List<Category> categories, Boolean mfa) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.score = score;
        this.getCategories().addAll(categories);
        this.mfa = mfa;
    }


    public User() {

    }

    public List<String> getRoleList() {
        if (this.roles.length() > 0) {
            return Arrays.asList(this.roles.split(","));
        }
        return new ArrayList<>();
    }

    public List<String> getPermissionList() {
        if (this.permissions.length() > 0) {
            return Arrays.asList(this.permissions.split(","));
        }
        return new ArrayList<>();
    }
}
