package nl.fhict.qaapi.model;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Entity
@Table(name = "question")
public class Question {
    @JsonView(View.External.Question.class)
    @Id
    @GeneratedValue
    @Getter
    @Setter
    private long id;

    @JsonView(View.External.Question.class)
    @Column(name = "date")
    @Getter
    @Setter
    private LocalDateTime date = LocalDateTime.now();

    @JsonView(View.External.Question.class)
    @Column(name = "title")
    @Getter
    @Setter
    private String title;

    @JsonView(View.External.Question.class)
    @Column(name = "description")
    @Getter
    @Setter
    private String description;

    @JsonView(View.External.Question.class)
    @Column(name = "upvotes")
    @Getter
    @Setter
    private int upvotes;

    @JsonView(View.External.Question.class)
    @Column(name = "downvotes")
    @Getter
    @Setter
    private int downvotes;

    @JsonView(View.External.Question.class)
    @Column(name = "views")
    @Getter
    @Setter
    private Long views = 0L;

    @JsonView(View.External.Question.class)
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "question_cateogries",
            joinColumns = @JoinColumn(name = "question_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "category_id", referencedColumnName = "id")
    )
    private Set<Category> categories = new HashSet<>();

    @JsonView(View.External.Question.class)
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Getter
    @Setter
    private User user;

    @JsonView(View.External.Question.class)
    @OneToMany(mappedBy = "question", cascade = CascadeType.REMOVE, orphanRemoval = true)
    @JsonManagedReference
    @Getter
    @Setter
    private List<Answer> answers;

    @JsonView(View.External.Question.class)
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "question_downvotes",
            joinColumns = @JoinColumn(name = "question_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id")
    )
    private Set<User> downvotedBy;

    @JsonView(View.External.Question.class)
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "question_upvotes",
            joinColumns = @JoinColumn(name = "question_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id")
    )
    private Set<User> upvotedBy;

    public Question(String title, String description, User user) {
        this.title = title;
        this.description = description;
        this.upvotes = 0;
        this.downvotes = 0;
        this.user = user;
    }

    public Question() {

    }
}
