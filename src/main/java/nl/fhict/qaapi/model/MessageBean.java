package nl.fhict.qaapi.model;

import lombok.Data;

@Data
public class MessageBean {
    private String name;
    private String message;
}
