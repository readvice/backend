package nl.fhict.qaapi.model;

public class View {
    public static interface External {
        interface User {
        }
        interface Question {
        }
        interface Answer {

        }
        interface Category {

        }
        interface UserQuestion extends User, Question {
        }
        interface QuestionAnswer extends Question, Answer {

        }
        interface UserQuestionAnswer extends User, Question, Answer {

        }
        interface UserAnswer extends User, Answer {

        }
        interface UserCategory extends User, Category {

        }
    }
    public static interface Internal extends External {
        interface User {
        }
        interface Question {
        }
        interface Answer {
        }
        interface Category {

        }
    }
}
