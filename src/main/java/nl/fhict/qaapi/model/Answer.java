package nl.fhict.qaapi.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "answer")
public class Answer {

    @JsonView(View.External.Answer.class)
    @Id
    @GeneratedValue
    private long id;

    @JsonView(View.External.Answer.class)
    @Column(name = "date")
    private LocalDateTime date = LocalDateTime.now();

    @JsonView(View.External.Answer.class)
    @Column(name = "text")
    private String text;

    @JsonView(View.External.Answer.class)
    @Column(name = "upvotes")
    private int upvotes;

    @JsonView(View.External.Answer.class)
    @Column(name = "downvotes")
    private int downvotes;

    @JsonView(View.External.Answer.class)
    @ManyToOne(optional = false)
    @JoinColumn(name = "question_id")
    @JsonBackReference
    private Question question;

    @JsonView(View.External.Answer.class)
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Getter
    @Setter
    private User user;

    public Answer(String text, Question question) {
        this.text = text;
        this.upvotes = 0;
        this.downvotes = 0;
        this.question = question;
    }

    public Answer() {

    }

}
