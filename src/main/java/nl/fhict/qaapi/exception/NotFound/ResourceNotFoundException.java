package nl.fhict.qaapi.exception.NotFound;


public class ResourceNotFoundException extends RuntimeException {
    private static final long serialVersionUID=1L;

    public ResourceNotFoundException(String resource) {
        super(resource + " not found!");
    }

    public ResourceNotFoundException(String resource, Long id) {
        super(resource + " not found! ID: " + id);
    }
    public ResourceNotFoundException(String resource, String param, String value) {
        super(resource + " not found! " + param + ": " + value);
    }
}
