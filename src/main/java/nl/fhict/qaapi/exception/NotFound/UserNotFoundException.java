package nl.fhict.qaapi.exception.NotFound;

public class UserNotFoundException extends ResourceNotFoundException {

    public UserNotFoundException() {
        super("User");
    }

    public UserNotFoundException(Long id) {
        super("User", id);
    }
    public UserNotFoundException(String param, String value) {
        super("User", param, value);
    }
}
