package nl.fhict.qaapi.exception.NotFound;

public class QuestionNotFoundException extends ResourceNotFoundException {

    public QuestionNotFoundException() {
        super("Question");
    }

    public QuestionNotFoundException(Long id) {
        super("Question", id);
    }
}
