package nl.fhict.qaapi.exception.NotFound;

public class ZeroResultsException extends RuntimeException {
    private static final long serialVersionUID=1L;

    public ZeroResultsException() {
        super("Zero results found for that query!");
    }

}
