package nl.fhict.qaapi.exception.NotFound;

public class AnswerNotFoundException extends ResourceNotFoundException {

    public AnswerNotFoundException() {
        super("Answer");
    }

    public AnswerNotFoundException(Long id) {
        super("Answer", id);
    }
}
