package nl.fhict.qaapi.exception.Forbidden;

public enum NonOwnerType {
    ACCOUNT, QUESTION, ANSWER
}
