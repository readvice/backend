package nl.fhict.qaapi.exception.Forbidden;

public class ForbiddenException extends RuntimeException{

    public ForbiddenException(String message) {
        super(message);
    }
}
