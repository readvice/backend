package nl.fhict.qaapi.exception.Forbidden;

public class NonOwnerException extends ForbiddenException{

    public NonOwnerException(NonOwnerType item) {
        super("You are not the owner of this " + item + "!");
    }
}
