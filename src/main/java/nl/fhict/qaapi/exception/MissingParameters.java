package nl.fhict.qaapi.exception;

public class MissingParameters extends IllegalArgumentException {

    public MissingParameters(String item) {
        super("You are missing a " + item + "!");
    }
}
