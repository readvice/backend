package nl.fhict.qaapi.service;

import nl.fhict.qaapi.model.Question;

import java.util.List;

public interface QuestionService {

    List<Question> findAll();

    Question findById(Long id);
}
