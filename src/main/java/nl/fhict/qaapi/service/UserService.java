package nl.fhict.qaapi.service;

import nl.fhict.qaapi.model.User;
import nl.fhict.qaapi.security.dto.LoginRequest;
import nl.fhict.qaapi.security.dto.LoginResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UserService {

    List<User> findAll();

    User findById(Long id);

    User findByUsername(String username);

    ResponseEntity<LoginResponse> login(LoginRequest loginRequest, String accessToken, String refreshToken);

    ResponseEntity<LoginResponse> refresh(String accessToken, String refreshToken);

    void logout(HttpHeaders httpHeaders);
}
