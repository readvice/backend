package nl.fhict.qaapi.security;

public class JwtProperties {
    public static final String SECRET = "testsecret";
    public static final String EXPIRATION_TIME = "" + 3600000L; //1h
    public static final String REFRESH_EXPIRATION_TIME = "" + 7776000000L; //90d
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String ACCESS_TOKEN_COOKIE_NAME = "Authorization";
    public static final String REFRESH_TOKEN_COOKIE_NAME = "Authorization";

}
