package nl.fhict.qaapi.security.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LoginResponse {
    public SuccessFailure status;
    private String message;

    public enum SuccessFailure {
        SUCCESS, FAILURE, MFA
    }
}
