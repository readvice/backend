package nl.fhict.qaapi.security.controller;

import nl.fhict.qaapi.exception.UnauthorizedException;
import nl.fhict.qaapi.security.JwtProperties;
import nl.fhict.qaapi.security.dto.LoginRequest;
import nl.fhict.qaapi.security.dto.LoginResponse;
import nl.fhict.qaapi.security.service.TotpManager;
import nl.fhict.qaapi.security.util.SecurityCipher;
import nl.fhict.qaapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;

@RestController
@RequestMapping("/api/")
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserService userService;

    @Autowired
    private TotpManager totpManager;

    @PostMapping(value = "/login", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object login(
            @CookieValue(name = JwtProperties.ACCESS_TOKEN_COOKIE_NAME, required = false) String accessToken,
            @CookieValue(name = JwtProperties.REFRESH_TOKEN_COOKIE_NAME, required = false) String refreshToken,
            @Valid @RequestBody LoginRequest loginRequest
    ) {


        try {
            userService.findByUsername(loginRequest.getUsername());
        } catch (NoSuchElementException ex) {
            throw new UnauthorizedException("Username or password is incorrect!");
        }


        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);

        String decryptedAccessToken = SecurityCipher.decrypt(accessToken);
        String decryptedRefreshToken = SecurityCipher.decrypt(refreshToken);

        if (Objects.requireNonNull(userService.login(loginRequest, decryptedAccessToken, decryptedRefreshToken).getBody()).status.toString().equals("SUCCESS")) {
            if (userService.findByUsername(loginRequest.getUsername()).getMfa() != null && userService.findByUsername(loginRequest.getUsername()).getMfa()) {
                return LoginResponse.SuccessFailure.MFA;
            }
            return userService.login(loginRequest, decryptedAccessToken, decryptedRefreshToken);
        } else {
            return new UnauthorizedException("Username or password is incorrect!");
        }

    }

    @PostMapping(value = "/verify")
    public Object verify(@CookieValue(name = JwtProperties.ACCESS_TOKEN_COOKIE_NAME, required = false) String accessToken,
                         @CookieValue(name = JwtProperties.REFRESH_TOKEN_COOKIE_NAME, required = false) String refreshToken,
                         @Valid @RequestBody LoginRequest loginRequest,
                         @RequestParam String code) {


        Map<String, String> secrets = userService.findByUsername(loginRequest.getUsername()).getSecrets();


        boolean verified = false;

        for (Map.Entry<String, String> secretEntry : secrets.entrySet()) {
            String secret = secretEntry.getValue();
            if(totpManager.verifyCode(secret, code)) {
                verified = true;
            }
        }

        if (!verified) {
            throw new UnauthorizedException("Code is incorrect!");
        }

        String decryptedAccessToken = SecurityCipher.decrypt(accessToken);
        String decryptedRefreshToken = SecurityCipher.decrypt(refreshToken);
        return userService.login(loginRequest, decryptedAccessToken, decryptedRefreshToken);
    }



    @PostMapping(value = "/refresh", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LoginResponse> refreshToken(@CookieValue(name = JwtProperties.ACCESS_TOKEN_COOKIE_NAME, required = false) String accessToken,
                                                      @CookieValue(name = JwtProperties.REFRESH_TOKEN_COOKIE_NAME, required = false) String refreshToken) {
        String decryptedAccessToken = SecurityCipher.decrypt(accessToken);
        String decryptedRefreshToken = SecurityCipher.decrypt(refreshToken);
        return userService.refresh(decryptedAccessToken, decryptedRefreshToken);
    }
}
