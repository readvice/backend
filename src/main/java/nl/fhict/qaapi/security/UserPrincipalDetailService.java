package nl.fhict.qaapi.security;

import nl.fhict.qaapi.exception.NotFound.UserNotFoundException;
import nl.fhict.qaapi.repository.UserRepository;
import nl.fhict.qaapi.security.dto.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
public class UserPrincipalDetailService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    public UserPrincipalDetailService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String s) {

        return new UserPrincipal(this.userRepository.findByUsername(s).orElseThrow((() -> new UserNotFoundException("Username", s))));
    }
}
