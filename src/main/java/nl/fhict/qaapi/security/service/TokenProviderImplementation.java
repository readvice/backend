package nl.fhict.qaapi.security.service;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import nl.fhict.qaapi.security.JwtProperties;
import nl.fhict.qaapi.security.dto.Token;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;

@Service
public class TokenProviderImplementation implements TokenProvider {
    @Value(JwtProperties.SECRET)
    private String tokenSecret;

    @Value(JwtProperties.EXPIRATION_TIME)
    private Long tokenExpirationMsec;

    @Value(JwtProperties.REFRESH_EXPIRATION_TIME)
    private Long refreshTokenExpirationMsec;

    private static final Logger LOGGER = LoggerFactory.getLogger(TokenProviderImplementation.class);


    Algorithm algorithm = Algorithm.HMAC512(JwtProperties.SECRET);

    @Override
    public Token generateAccessToken(String subject) {
        Date now = new Date();
        Long duration = now.getTime() + tokenExpirationMsec;
        Date expiryDate = new Date(duration);

        String token = JWT.create()
                .withSubject(subject)
//                .withClaim("email", ((UserPrincipal) auth.getPrincipal()).getEmail())
                .withIssuedAt(now)
                .withExpiresAt(expiryDate)
                .sign(HMAC512(JwtProperties.SECRET.getBytes()));

        return new Token(Token.TokenType.ACCESS, token, duration, LocalDateTime.ofInstant(expiryDate.toInstant(), ZoneId.systemDefault()));
    }

    @Override
    public Token generateRefreshToken(String subject) {
        Date now = new Date();
        Long duration = now.getTime() + refreshTokenExpirationMsec;
        Date expiryDate = new Date(duration);

        String token = JWT.create()
                .withSubject(subject)
//                .withClaim("email", ((UserPrincipal) auth.getPrincipal()).getEmail())
                .withIssuedAt(now)
                .withExpiresAt(expiryDate)
                .sign(HMAC512(JwtProperties.SECRET.getBytes()));
        return new Token(Token.TokenType.REFRESH, token, duration, LocalDateTime.ofInstant(expiryDate.toInstant(), ZoneId.systemDefault()));
    }

    @Override
    public String getUsernameFromToken(String token) {
        JWTVerifier verifier = JWT.require(algorithm)
              .build();
        DecodedJWT jwt = verifier.verify(token);

        return jwt.getSubject();

    }

    @Override
    public LocalDateTime getExpiryDateFromToken(String token) {
        JWTVerifier verifier = JWT.require(algorithm)
                .build();
        DecodedJWT jwt = verifier.verify(token);

        return LocalDateTime.ofInstant(jwt.getExpiresAt().toInstant(), ZoneId.systemDefault());
    }

    @Override
    public boolean validateToken(String token) {

        if (token == null) {
            return false;
        }

        try {

            JWTVerifier verifier = JWT.require(algorithm)
                    .build(); //Reusable verifier instance
            DecodedJWT jwt = verifier.verify(token);

            Date now = new Date();

            return !jwt.getExpiresAt().before(now);

        } catch (JWTVerificationException e){
            LOGGER.error(e.getMessage());
        }

        return false;
    }
}
