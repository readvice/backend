package nl.fhict.qaapi.controller;

import com.fasterxml.jackson.annotation.JsonView;
import nl.fhict.qaapi.exception.ConflictException;
import nl.fhict.qaapi.exception.Forbidden.NonOwnerException;
import nl.fhict.qaapi.exception.Forbidden.NonOwnerType;
import nl.fhict.qaapi.exception.NotFound.UserNotFoundException;
import nl.fhict.qaapi.exception.NotFound.ZeroResultsException;
import nl.fhict.qaapi.exception.UnauthorizedException;
import nl.fhict.qaapi.model.Question;
import nl.fhict.qaapi.model.User;
import nl.fhict.qaapi.model.View;
import nl.fhict.qaapi.repository.QuestionRepository;
import nl.fhict.qaapi.repository.UserRepository;
import nl.fhict.qaapi.security.service.TotpManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@RestController
@RequestMapping("/api/")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private TotpManager totpManager;

    //CREATE
    @PostMapping("/users")
    @JsonView(View.External.User.class)
    public ResponseEntity<User> newUser(@RequestBody User newUser) {
        if (userRepository.existsUserByUsername(newUser.getUsername())) {
            throw new ConflictException("Username is already taken!");
        } else {
            userRepository.save(newUser);
            return new ResponseEntity<>(newUser, HttpStatus.CREATED);
        }
    }

    //READ
    @GetMapping("/users")
    @JsonView(View.External.User.class)
    public ResponseEntity<List<User>> getAllUsers() {
        List<User> users = userRepository.findAll();
        if (users.isEmpty()) {
            throw new ZeroResultsException();
        }
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @GetMapping("/users-internal")
    @JsonView(View.Internal.User.class)
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<List<User>> getAllUsersAsAdmin() {
        List<User> users = userRepository.findAll();
        if (users.isEmpty()) {
            throw new ZeroResultsException();
        }
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    //READ
    @GetMapping("/users/{id}")
    @JsonView(View.External.User.class)
    public ResponseEntity<User> getOneUser(@PathVariable long id) {
        User u;
        try {
            u = userRepository.findById(id).get();
        } catch (NoSuchElementException ex) {
            throw new UserNotFoundException(id);
        }
        return new ResponseEntity<>(u, HttpStatus.OK);
    }

    @GetMapping("/users/{id}/questions")
    @JsonView(View.External.UserQuestionAnswer.class)
    public Page<Question> getAllQuestionsByUserId(@PathVariable long id, Pageable pageable) {
        return questionRepository.findByUserId(id, pageable);
    }

    @GetMapping("/users/me")
    @PreAuthorize("isAuthenticated()")
    public Object getMe(HttpServletRequest httpServletRequest, Authentication authentication) {
        return httpServletRequest.getUserPrincipal();
    }

    //UPDATE


    //2FA
    String secret;

    @GetMapping("/users/{id}/2fa/setup")
    @PreAuthorize("isAuthenticated()")
    public Object create2fa(@PathVariable Long id, HttpServletRequest httpServletRequest) {
        userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));

        if (isOwner(id, httpServletRequest)) {
            secret = totpManager.generateSecret();
            Map<String, Object> rtn = new LinkedHashMap<>();

            rtn.put("username", httpServletRequest.getUserPrincipal().getName());
            rtn.put("secret", secret);
            rtn.put("issuer", "readvice");
            rtn.put("algorithm", "SHA1");
            rtn.put("digits", 6);
            rtn.put("period", 30);

            return ResponseEntity.ok().body(rtn);
        } else {
            throw new NonOwnerException(NonOwnerType.ACCOUNT);
        }
    }

    @PostMapping("/users/{id}/2fa/verify")
    @PreAuthorize("isAuthenticated()")
    public Object verify2fa(@PathVariable Long id, @RequestParam String keyname, @RequestParam String code, HttpServletRequest httpServletRequest) {
        User user = userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));

        if (isOwner(id, httpServletRequest)) {
            if (totpManager.verifyCode(secret, code)) {
                if (user.getSecrets().containsKey(keyname)) {
                    throw new ConflictException("Key with that name already exists!");
                }
                user.getSecrets().put(keyname, secret);
                user.setMfa(true);
                userRepository.save(user);
                Map<String, Object> rtn = new LinkedHashMap<>();

                rtn.put("keyname", keyname);
                return ResponseEntity.ok().body(rtn);
            }
            //code is incorrect
            throw new UnauthorizedException("Incorrect code!");
        } else {
            throw new NonOwnerException(NonOwnerType.ACCOUNT);
        }
    }

    @GetMapping("/users/{id}/2fa")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Set<String>> getTokens(@PathVariable long id, HttpServletRequest httpServletRequest) {
        User user = userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));

        if (isOwner(id, httpServletRequest)) {
            return new ResponseEntity<>(user.getSecrets().keySet(), HttpStatus.OK);
        } else {
            throw new NonOwnerException(NonOwnerType.ACCOUNT);
        }
    }

    @DeleteMapping("/users/{id}/2fa/{key}")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Void> deleteKey(@PathVariable long id, HttpServletRequest httpServletRequest, @PathVariable String key) {
        User user = userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));

        if (isOwner(id, httpServletRequest)) {
            user.getSecrets().remove(key);
            userRepository.save(user);
            if (user.getSecrets().size() < 1) {
                user.setMfa(false);
            }
            userRepository.save(user);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            throw new NonOwnerException(NonOwnerType.ACCOUNT);
        }
    }

    //DELETE
    @DeleteMapping("/users/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Void> deleteUser(@PathVariable long id) {
        userRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


    private boolean isOwner(Long id, HttpServletRequest httpServletRequest) {
        return userRepository.findById(id).get().getUsername().equals(httpServletRequest.getUserPrincipal().getName());
    }

}
