package nl.fhict.qaapi.controller;

import com.fasterxml.jackson.annotation.JsonView;
import nl.fhict.qaapi.exception.Forbidden.NonOwnerException;
import nl.fhict.qaapi.exception.Forbidden.NonOwnerType;
import nl.fhict.qaapi.exception.MissingParameters;
import nl.fhict.qaapi.exception.NotFound.QuestionNotFoundException;
import nl.fhict.qaapi.exception.NotFound.UserNotFoundException;
import nl.fhict.qaapi.exception.NotFound.ZeroResultsException;
import nl.fhict.qaapi.model.Question;
import nl.fhict.qaapi.model.User;
import nl.fhict.qaapi.model.View;
import nl.fhict.qaapi.repository.QuestionRepository;
import nl.fhict.qaapi.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/api/")
public class QuestionController {
    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private UserRepository userRepository;

    //CREATE
    @PostMapping("/questions")
    @JsonView(View.External.UserQuestionAnswer.class)
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Question> newQuestion(@RequestBody Question question, HttpServletRequest httpServletRequest) {
        User u;
        try {
            u = userRepository.findById(question.getUser().getId()).get();
        } catch (NoSuchElementException ex) {
            throw new UserNotFoundException("ID", String.valueOf(question.getUser().getId()));
        } catch (NullPointerException ex) {
            throw new MissingParameters("user");
        }

        question.setUser(u);
        questionRepository.save(question);
        return new ResponseEntity<>(question, HttpStatus.CREATED);
    }

    //READ
    @GetMapping("/questions/search")
    @JsonView(View.External.UserQuestionAnswer.class)
    public ResponseEntity<List<Question>> getAllQuestionsByQuery(@RequestParam String query) {
        List<Question> questions = questionRepository.findByTitleContainsOrDescriptionContains(query, query);
        if (questions.isEmpty()) {
            throw new ZeroResultsException();
        }
        return new ResponseEntity<>(questions, HttpStatus.OK);
    }

    @GetMapping("/questions")
    @JsonView(View.External.UserQuestionAnswer.class)
    public ResponseEntity<List<Question>> getAllQuestions() {
        List<Question> questions = questionRepository.findAll();
        if (questions.isEmpty()) {
            throw new ZeroResultsException();
        }
        return new ResponseEntity<>(questions, HttpStatus.OK);
    }


    //READ
    @GetMapping("/questions/{id}")
    @JsonView(View.External.UserQuestionAnswer.class)
    public ResponseEntity<Question> getOneQuestion(@PathVariable long id) {
        Question q;
        try {
            q = questionRepository.findById(id).get();
        } catch (NoSuchElementException ex) {
            throw new QuestionNotFoundException(id);
        }
        return new ResponseEntity<>(q, HttpStatus.OK);
    }

    @PutMapping("/questions/{id}/downvote")
    @JsonView(View.External.UserQuestionAnswer.class)
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Question> downvote(@PathVariable Long id, HttpServletRequest httpServletRequest) {
        Question q;
        try {
            q = questionRepository.findById(id).get();
        } catch (NoSuchElementException ex) {
            throw new QuestionNotFoundException(id);
        }

        User u;
        try {
            u = userRepository.findByUsername(httpServletRequest.getUserPrincipal().getName()).get();
        } catch (NoSuchElementException ex) {
            throw new UserNotFoundException("Username", httpServletRequest.getUserPrincipal().getName());
        } catch (NullPointerException ex) {
            throw new MissingParameters("user");
        }

        if (q.getDownvotedBy().contains(u)) {
            q.getDownvotedBy().remove(u);
            u.getDownvotes().remove(q);
        } else {
            q.getDownvotedBy().add(u);
            u.getDownvotes().add(q);
        }

        q.getUpvotedBy().remove(u);
        u.getUpvotes().remove(q);
        questionRepository.save(q);
        userRepository.save(u);
        return new ResponseEntity<>(q, HttpStatus.OK);

    }

    @PutMapping("/questions/{id}/upvote")
    @JsonView(View.External.UserQuestionAnswer.class)
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Question> upvote(@PathVariable Long id, HttpServletRequest httpServletRequest) {
        Question q;
        try {
            q = questionRepository.findById(id).get();
        } catch (NoSuchElementException ex) {
            throw new QuestionNotFoundException(id);
        }

        User u;
        try {
            u = userRepository.findByUsername(httpServletRequest.getUserPrincipal().getName()).get();
        } catch (NoSuchElementException ex) {
            throw new UserNotFoundException("Username", httpServletRequest.getUserPrincipal().getName());
        } catch (NullPointerException ex) {
            throw new MissingParameters("user");
        }

        if (q.getDownvotedBy().contains(u)) {
            q.getUpvotedBy().remove(u);
            u.getUpvotes().remove(q);
        } else {
            q.getUpvotedBy().add(u);
            u.getUpvotes().add(q);
        }

        q.getDownvotedBy().remove(u);
        u.getDownvotes().remove(q);
        questionRepository.save(q);
        userRepository.save(u);
        return new ResponseEntity<>(q, HttpStatus.OK);

    }


    //UPDATE
    @PutMapping("/questions/{id}")
    @JsonView(View.External.UserQuestionAnswer.class)
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Question> updateQuestion(@PathVariable Long id, @RequestBody Question questionRequest, HttpServletRequest httpServletRequest) {
        Question q;
        try {
            q = questionRepository.findById(id).get();
        } catch (NoSuchElementException ex) {
            throw new QuestionNotFoundException(id);
        }

        if (isOwner(q.getUser().getId(), httpServletRequest)) {
            q.setTitle(questionRequest.getTitle());
            q.setDescription(questionRequest.getDescription());
            questionRepository.save(q);
            return new ResponseEntity<>(q, HttpStatus.OK);
        } else {
            throw new NonOwnerException(NonOwnerType.QUESTION);
        }
    }

    //DELETE
    @DeleteMapping("/questions/{id}")
    @PreAuthorize("isAuthenticated()")
    public void deleteQuestion(@PathVariable long id, HttpServletRequest httpServletRequest) {
        Question q = questionRepository.findById(id).orElseThrow(() -> new QuestionNotFoundException(id));

        if (isOwner(q.getUser().getId(), httpServletRequest)) {
            questionRepository.delete(q);
        } else {
            throw new NonOwnerException(NonOwnerType.QUESTION);
        }
    }

    public boolean isOwner(Long id, HttpServletRequest httpServletRequest) {
        return userRepository.findById(id).get().getUsername().equals(httpServletRequest.getUserPrincipal().getName());
    }
}
