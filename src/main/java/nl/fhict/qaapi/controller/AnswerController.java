package nl.fhict.qaapi.controller;

import com.fasterxml.jackson.annotation.JsonView;
import nl.fhict.qaapi.exception.Forbidden.NonOwnerException;
import nl.fhict.qaapi.exception.Forbidden.NonOwnerType;
import nl.fhict.qaapi.exception.NotFound.AnswerNotFoundException;
import nl.fhict.qaapi.exception.NotFound.QuestionNotFoundException;
import nl.fhict.qaapi.exception.NotFound.UserNotFoundException;
import nl.fhict.qaapi.model.Answer;
import nl.fhict.qaapi.model.Question;
import nl.fhict.qaapi.model.User;
import nl.fhict.qaapi.model.View;
import nl.fhict.qaapi.repository.AnswerRepository;
import nl.fhict.qaapi.repository.QuestionRepository;
import nl.fhict.qaapi.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/api/")
public class AnswerController {
    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private UserRepository userRepository;

    //CREATE
    @PostMapping("/questions/{questionId}/answers")
    @JsonView(View.External.UserAnswer.class)
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Answer> createAnswer(@PathVariable Long questionId, @RequestBody Answer answer, HttpServletRequest httpServletRequest) {
        Question q;
        try {
            q = questionRepository.findById(questionId).get();
        } catch (NoSuchElementException ex) {
            throw new QuestionNotFoundException(questionId);
        }

        User u;
        try {
            u = userRepository.findByUsername(httpServletRequest.getUserPrincipal().getName()).get();
        } catch (NoSuchElementException ex) {
            throw new UserNotFoundException("Username", httpServletRequest.getUserPrincipal().getName());
        }

        answer.setQuestion(q);
        answer.setUser(u);
        answerRepository.save(answer);
        return new ResponseEntity<>(answer, HttpStatus.CREATED);
    }

    //READ
    @GetMapping("/questions/{questionId}/answers")
    @JsonView(View.External.UserAnswer.class)
    public Page<Answer> getAllCommentsByPostId(@PathVariable(value = "questionId") Long questionId,
                                               Pageable pageable) {
        return answerRepository.findByQuestionId(questionId, pageable);
    }

    //UPDATE
    @PutMapping("/questions/{questionId}/answers/{answerId}")
    @JsonView(View.External.UserAnswer.class)
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Answer> updateAnswer(@PathVariable long questionId, @PathVariable long answerId, @RequestBody Answer answer, HttpServletRequest httpServletRequest) {
        Answer a;
        try {
            a = answerRepository.findById(answerId).get();
        } catch (NoSuchElementException ex) {
            throw new AnswerNotFoundException(answerId);
        }

        if (isOwner(a.getUser().getId(), httpServletRequest)) {
            a.setText(answer.getText());
            answerRepository.save(a);
            return new ResponseEntity<>(a, HttpStatus.OK);
        } else {
            throw new NonOwnerException(NonOwnerType.ANSWER);
        }
    }

    //DELETE
    @DeleteMapping("/questions/{questionId}/answers/{answerId}")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Answer> deleteAnswer(@PathVariable long questionId, @PathVariable long answerId, HttpServletRequest httpServletRequest) {
        Answer a;
        try {
            a = answerRepository.findById(answerId).get();
        } catch (NoSuchElementException ex) {
            throw new AnswerNotFoundException(answerId);
        }

        if (isOwner(a.getUser().getId(), httpServletRequest)) {
            answerRepository.deleteById(answerId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            throw new NonOwnerException(NonOwnerType.ANSWER);
        }

    }

    public boolean isOwner(Long id, HttpServletRequest httpServletRequest) {
        return userRepository.findById(id).get().getUsername().equals(httpServletRequest.getUserPrincipal().getName());
    }

}
