package nl.fhict.qaapi.repository;

import nl.fhict.qaapi.model.Question;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {
    Page<Question> findByUserId(Long userId, Pageable pageable);
    List<Question> findByTitleContainsOrDescriptionContains(String title, String description);
}
