package nl.fhict.qaapi.service;

import nl.fhict.qaapi.model.Question;
import nl.fhict.qaapi.model.User;
import nl.fhict.qaapi.repository.QuestionRepository;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
class QuestionServiceImplementationTest {

    @InjectMocks
    private QuestionServiceImplementation questionServiceImplementation;

    @Mock
    private QuestionRepository questionRepository;

    @Test
    void findAll() {
        when(questionRepository.findAll()).thenReturn(Stream.of(
                new Question("What is?", "Description", new User()),
                new Question("How is?", "Lorem Ipsum", new User())
        ).collect(Collectors.toList()));

        List<Question> userList = questionServiceImplementation.findAll();

        assertEquals(2, userList.size());
        verify(questionRepository, times(1)).findAll();
    }
}
