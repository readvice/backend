//package nl.fhict.qaapi;
//
//import nl.fhict.qaapi.controller.QuestionController;
//import nl.fhict.qaapi.model.Question;
//import nl.fhict.qaapi.model.User;
//import nl.fhict.qaapi.repository.QuestionRepository;
//import nl.fhict.qaapi.service.QuestionServiceImplementation;
//import org.junit.jupiter.api.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.util.stream.Collectors;
//import java.util.stream.Stream;
//
//import static org.junit.jupiter.api.Assertions.*;
//import static org.mockito.Mockito.when;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest
//class qaapiTests {
//
//    @Autowired
//    private QuestionController controller;
//
//    @MockBean
//    private QuestionServiceImplementation questionServiceImplementation;
//
//    @Test
//    public void getAllQuestionsTest() {
//        when(questionServiceImplementation.findAll()).thenReturn(Stream.of(
//            new Question("What is?", "Description", new User()),
//            new Question("How is?", "Lorem Ipsum", new User())
//        ).collect(Collectors.toList()));
//        assertEquals(2, questionServiceImplementation.findAll().size());
//    }
//
//}
