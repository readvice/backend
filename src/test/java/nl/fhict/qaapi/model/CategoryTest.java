package nl.fhict.qaapi.model;

import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class CategoryTest {

    Category c = new Category();

    @Test
    void getId() {
        c.setId(2);
        assertEquals(2, c.getId());
    }

    @Test
    void getName() {
        c.setName("test");
        assertEquals("test", c.getName());
    }

    @Test
    void getUsers() {
        Set<User> userSet = new HashSet<>();
        c.setUsers(userSet);
        assertEquals(userSet, c.getUsers());
    }

    @Test
    void setId() {
        c.setId(2);
        assertEquals(c.getId(), 2);
    }

    @Test
    void setName() {
        c.setName("test");
        assertEquals(c.getName(), "test");
    }

    @Test
    void setUsers() {
        Set<User> userSet = new HashSet<>();
        c.setUsers(userSet);
        assertEquals(c.getUsers(), userSet);
    }
}
