package nl.fhict.qaapi.model;

import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class UserTest {

    User u = new User();

    @Test
    void setId() {
        u.setId(2);
        assertEquals(u.getId(), 2);
    }

    @Test
    void setUsername() {
        u.setUsername("test");
        assertEquals(u.getUsername(), "test");
    }

    @Test
    void setEmail() {
        u.setEmail("test");
        assertEquals(u.getEmail(), "test");
    }

    @Test
    void setPassword() {
        u.setPassword("test");
        assertEquals(u.getPassword(), "test");
    }

    @Test
    void setScore() {
        u.setScore(2);
        assertEquals(u.getScore(), 2);
    }

    @Test
    void setCategories() {
        Set<Category> categorySet = new HashSet<>();
        u.setCategories(categorySet);
        assertEquals(u.getCategories(), categorySet);
    }

    @Test
    void setMfa() {
        u.setMfa(true);
        assertEquals(u.getMfa(), true);
    }

    @Test
    void setSecrets() {
        Map<String, String> secrets = new LinkedHashMap<>();
        u.setSecrets(secrets);
        assertEquals(u.getSecrets(), secrets);
    }

    @Test
    void setRoles() {
        u.setRoles("test");
        assertEquals(u.getRoles(), "test");
    }

    @Test
    void setPermissions() {
        u.setPermissions("test");
        assertEquals(u.getPermissions(), "test");
    }

    @Test
    void setQuestions() {
        List<Question> questionList = new ArrayList<>();
        u.setQuestions(questionList);
        assertEquals(u.getQuestions(), questionList);
    }

    @Test
    void setDownvotes() {
        List<Question> questionList = new ArrayList<>();
        u.setDownvotes(questionList);
        assertEquals(u.getDownvotes(), questionList);
    }

    @Test
    void setUpvotes() {
        List<Question> questionList = new ArrayList<>();
        u.setUpvotes(questionList);
        assertEquals(u.getUpvotes(), questionList);
    }

    @Test
    void getId() {
        u.setId(2);
        assertEquals(2, u.getId());
    }

    @Test
    void getUsername() {
        u.setUsername("test");
        assertEquals("test", u.getUsername());
    }

    @Test
    void getEmail() {
        u.setEmail("test");
        assertEquals("test", u.getEmail());
    }

    @Test
    void getPassword() {
        u.setPassword("test");
        assertEquals("test", u.getPassword());
    }

    @Test
    void getScore() {
        u.setScore(2);
        assertEquals(2, u.getScore());
    }

    @Test
    void getCategories() {
        Set<Category> categorySet = new HashSet<>();
        u.setCategories(categorySet);
        assertEquals(categorySet, u.getCategories());
    }

    @Test
    void getMfa() {
        u.setMfa(true);
        assertEquals(true, u.getMfa());
    }

    @Test
    void getSecrets() {
        Map<String, String> secrets = new LinkedHashMap<>();
        u.setSecrets(secrets);
        assertEquals(secrets, u.getSecrets());
    }

    @Test
    void getRoles() {
        u.setRoles("test");
        assertEquals("test", u.getRoles());
    }

    @Test
    void getPermissions() {
        u.setPermissions("test");
        assertEquals("test", u.getPermissions());
    }

    @Test
    void getQuestions() {
        List<Question> questionList = new ArrayList<>();
        u.setQuestions(questionList);
        assertEquals(questionList, u.getQuestions());
    }

    @Test
    void getDownvotes() {
        List<Question> questionList = new ArrayList<>();
        u.setDownvotes(questionList);
        assertEquals(questionList, u.getDownvotes());
    }

    @Test
    void getUpvotes() {
        List<Question> questionList = new ArrayList<>();
        u.setUpvotes(questionList);
        assertEquals(questionList, u.getUpvotes());
    }
}
