package nl.fhict.qaapi.model;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class QuestionTest {

    Question q = new Question();

    @Test
    void getCategories() {
        Set<Category> categorySet = new HashSet<>();
        q.setCategories(categorySet);
        assertEquals(categorySet, q.getCategories());
    }

    @Test
    void getDownvotedBy() {
        Set<User> userSet = new HashSet<>();
        q.setDownvotedBy(userSet);
        assertEquals(userSet, q.getDownvotedBy());
    }

    @Test
    void getUpvotedBy() {
        Set<User> userSet = new HashSet<>();
        q.setUpvotedBy(userSet);
        assertEquals(userSet, q.getUpvotedBy());
    }

    @Test
    void setCategories() {
        Set<Category> categorySet = new HashSet<>();
        q.setCategories(categorySet);
        assertEquals(q.getCategories(), categorySet);
    }

    @Test
    void setDownvotedBy() {
        Set<User> userSet = new HashSet<>();
        q.setDownvotedBy(userSet);
        assertEquals(q.getDownvotedBy(), userSet);
    }

    @Test
    void setUpvotedBy() {
        Set<User> userSet = new HashSet<>();
        q.setUpvotedBy(userSet);
        assertEquals(q.getUpvotedBy(), userSet);
    }

    @Test
    void getId() {
        q.setId(2);
        assertEquals(2, q.getId());
    }

    @Test
    void getDate() {
        q.setDate(LocalDateTime.parse("2021-02-20T06:30"));
        assertEquals(LocalDateTime.parse("2021-02-20T06:30:00"), q.getDate());
    }

    @Test
    void getTitle() {
        q.setTitle("test");
        assertEquals("test", q.getTitle());
    }

    @Test
    void getDescription() {
        q.setDescription("test");
        assertEquals("test", q.getDescription());
    }

    @Test
    void getUpvotes() {
        q.setUpvotes(20);
        assertEquals(20, q.getUpvotes());
    }

    @Test
    void getDownvotes() {
        q.setDownvotes(20);
        assertEquals(20, q.getDownvotes());
    }

    @Test
    void getViews() {
        q.setViews(20L);
        assertEquals(20L, q.getViews());
    }

    @Test
    void getUser() {
        User u = new User();
        q.setUser(u);
        assertEquals(u, q.getUser());
    }

    @Test
    void getAnswers() {
        List<Answer> answerList = new ArrayList<>();
        q.setAnswers(answerList);
        assertEquals(answerList, q.getAnswers());
    }

    @Test
    void setId() {
        q.setId(20);
        assertEquals(q.getId(), 20);
    }

    @Test
    void setDate() {
        q.setDate(LocalDateTime.parse("2021-02-20T06:30:00"));
        assertEquals(q.getDate(), "2021-02-20T06:30:00");
    }

    @Test
    void setTitle() {
        q.setTitle("test");
        assertEquals(q.getTitle(), "test");
    }

    @Test
    void setDescription() {
        q.setDescription("test");
        assertEquals(q.getDescription(), "test");
    }

    @Test
    void setUpvotes() {
        q.setUpvotes(20);
        assertEquals(q.getUpvotes(), 20);
    }

    @Test
    void setDownvotes() {
        q.setDownvotes(20);
        assertEquals(q.getDownvotes(), 20);
    }

    @Test
    void setViews() {
        q.setViews(20L);
        assertEquals(q.getViews(), 20);
    }

    @Test
    void setUser() {
        User u = new User();
        q.setUser(u);
        assertEquals(q.getUser(), u);
    }

    @Test
    void setAnswers() {
        List<Answer> answerList = new ArrayList<>();
        q.setAnswers(answerList);
        assertEquals(q.getAnswers(), answerList);
    }
}
