package nl.fhict.qaapi.model;

import org.apache.tomcat.jni.Local;
import org.junit.Before;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class AnswerTest {

    Answer a = new Answer();

    @Test
    void constructorTest() {
        a.setText("test");
        assertEquals("test", a.getText());
    }

    @Test
    void getId() {
        a.setId(2);
        assertEquals(2, a.getId());
    }

    @Test
    void getDate() {
        a.setDate(LocalDateTime.parse("2021-02-20T06:30"));
        assertEquals(LocalDateTime.parse("2021-02-20T06:30:00"), a.getDate());
    }

    @Test
    void getText() {
        a.setText("test");
        assertEquals("test", a.getText());
    }

    @Test
    void getUpvotes() {
        a.setUpvotes(20);
        assertEquals(20, a.getUpvotes());
    }

    @Test
    void getDownvotes() {
        a.setDownvotes(20);
        assertEquals(20, a.getDownvotes());
    }

    @Test
    void getQuestion() {
        Question q = new Question();
        a.setQuestion(q);
        assertEquals(q, a.getQuestion());
    }

    @Test
    void setId() {
        a.setId(20);
        assertEquals(a.getId(), 20);
    }

    @Test
    void setDate() {
        a.setDate(LocalDateTime.parse("2021-02-20T06:30:00"));
        assertEquals(a.getDate(), "2021-02-20T06:30:00");
    }

    @Test
    void setText() {
        a.setText("test");
        assertEquals(a.getText(), "test");
    }

    @Test
    void setUpvotes() {
        a.setUpvotes(20);
        assertEquals(a.getUpvotes(), 20);
    }

    @Test
    void setDownvotes() {
        a.setDownvotes(20);
        assertEquals(a.getDownvotes(), 20);
    }

    @Test
    void setQuestion() {
        Question q = new Question();
        a.setQuestion(q);
        assertEquals(a.getQuestion(), q);
    }

    @Test
    void getUser() {
        User u = new User();
        a.setUser(u);
        assertEquals(u, a.getUser());
    }

    @Test
    void setUser() {
        User u = new User();
        a.setUser(u);
        assertEquals(u, a.getUser());
    }
}
