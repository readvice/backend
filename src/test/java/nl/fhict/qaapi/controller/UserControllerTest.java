package nl.fhict.qaapi.controller;

import nl.fhict.qaapi.model.Question;
import nl.fhict.qaapi.model.User;
import nl.fhict.qaapi.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
class UserControllerTest {

    @InjectMocks
    UserController userController;

    @Mock
    UserRepository userRepository;


    @Test
    void newUser() {
    }

    @Test
    void getAllUsers() {
        // given
//        User employee1 = new User();
//        User employee2 = new User();
//
//        List<User> users = Arrays.asList(employee1, employee2);
//
//        when(userRepository.findAll()).thenReturn(users);
//
//        // when
//        ResponseEntity<List<User>> result = userController.getAllUsers();
//
//        // then
//        assertEquals(result.getBody().size(), 2);

    }

    @Test
    void getAllUsersAsAdmin() {
    }

    @Test
    void getOneUser() {
    }

    @Test
    void getAllQuestionsByUserId() {
    }

    @Test
    void getMe() {
    }

    @Test
    void create2fa() {
    }

    @Test
    void verify2fa() {
    }

    @Test
    void getTokens() {
    }

    @Test
    void deleteKey() {
    }

    @Test
    void deleteUser() {
    }
}
