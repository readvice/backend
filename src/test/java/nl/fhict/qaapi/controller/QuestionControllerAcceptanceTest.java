package nl.fhict.qaapi.controller;

import nl.fhict.qaapi.model.Question;
import nl.fhict.qaapi.model.User;
import nl.fhict.qaapi.repository.QuestionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

import static org.springframework.http.HttpStatus.*;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class QuestionControllerAcceptanceTest {

    @LocalServerPort
    int randomServerPort;

    private RestTemplate restTemplate;
    private String url;

    @BeforeEach
    void setUp() {
        restTemplate = new RestTemplate();
        url = "http://localhost:" + randomServerPort + "/api/questions";
    }

    @Test
    void getAllQuestions() {
        ResponseEntity responseEntity = restTemplate.getForEntity(url, String.class);
        assertEquals(NO_CONTENT, responseEntity.getStatusCode());
    }

}
